import 'package:mechuli/model/Food.dart';

class ListUtil {
  static List<dynamic> expandList(List<dynamic> originalList, int desiredLength) {
    List<dynamic> expandedList = List.from(originalList); // 원본 리스트를 복사합니다.

    while (expandedList.length < desiredLength) {
      // 원하는 길이에 도달할 때까지 리스트를 복제하고 확장합니다.
      expandedList.addAll(List.from(originalList));
    }

    // 원하는 길이에 도달한 경우, 추가된 요소들을 제거합니다.
    return expandedList.sublist(0, desiredLength);
  }
}
