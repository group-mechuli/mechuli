import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mechuli/data/Category.dart';
import 'package:mechuli/model/Food.dart';
import 'package:mechuli/screen/result_screen.dart';
import 'dart:convert';
import 'package:mechuli/screen/rollet_screen.dart';
import 'package:mechuli/constants/constants.dart';
import 'package:mechuli/util/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DataLoader {
  static Future<void> loadJsonData(BuildContext context, CategoryType categoryType, String type) async {
    try {
      // assets 폴더의 JSON 파일 로드
      String beforeString = await rootBundle.loadString('data/before.json');
      String jsonString = await rootBundle.loadString('data/data.json');
      String afterString = await rootBundle.loadString('data/after.json');

      // JSON 데이터 파싱
      List<dynamic> beforeData = json.decode(beforeString);
      List<dynamic> jsonData = json.decode(jsonString);
      List<dynamic> afterData = json.decode(afterString);
      List<Food> beforeFoods = beforeData.map((json) => Food.fromJson(json)).toList();
      List<Food> foods = jsonData.map((json) => Food.fromJson(json)).toList();
      List<Food> afterFoods = afterData.map((json) => Food.fromJson(json)).toList();

      List<Food> filterFoods = [];
      switch(categoryType) {
        case CategoryType.ALL:
          filterFoods = foods;
          break;
        case CategoryType.CHICKEN:
          filterFoods = foods.where((element) => element.category == CategoryType.CHICKEN.value).toList();
          break;
        case CategoryType.KOREAN:
          filterFoods = foods.where((element) => element.category == CategoryType.KOREAN.value).toList();
          break;
        case CategoryType.CHINA:
          filterFoods = foods.where((element) => element.category == CategoryType.CHINA.value).toList();
          break;
        case CategoryType.HAMBURGER:
          filterFoods = foods.where((element) => element.category == CategoryType.HAMBURGER.value).toList();
          break;
        case CategoryType.PIZZA:
          filterFoods = foods.where((element) => element.category == CategoryType.PIZZA.value).toList();
          break;
        case CategoryType.BOSSAM:
          filterFoods = foods.where((element) => element.category == CategoryType.BOSSAM.value).toList();
          break;
        case CategoryType.JJIM:
          filterFoods = foods.where((element) => element.category == CategoryType.JJIM.value).toList();
          break;
        case CategoryType.ASIAN:
          filterFoods = foods.where((element) => element.category == CategoryType.ASIAN.value).toList();
          break;
        case CategoryType.SUSHI:
          filterFoods = foods.where((element) => element.category == CategoryType.SUSHI.value).toList();
          break;
        case CategoryType.BUNSIK:
          filterFoods = foods.where((element) => element.category == CategoryType.BUNSIK.value).toList();
          break;
        case CategoryType.SALAD:
          filterFoods = foods.where((element) => element.category == CategoryType.SALAD.value).toList();
          break;
        default:
        break;
      }

      int startIndex = 0;
      int endIndex = 0;
      filterFoods.shuffle();
      List<Food> mergedList = [];
      if(filterFoods.length > 100) {
        filterFoods = filterFoods.sublist(0, 100);
        mergedList = filterFoods;
      } else {
        int num = 0;
        startIndex = (selectIndex-(filterFoods.length/2).floor() as int);
        endIndex =  (selectIndex+(filterFoods.length/2).floor() as int);
        mergedList = [...beforeFoods, ...afterFoods];
        mergedList = mergedList.sublist(0, 100);
        for (int i = startIndex; i < endIndex; i++) {
          mergedList[i] = filterFoods[num++];
        }
      }

      // SharedPreferences 인스턴스 얻기
      SharedPreferences prefs = await SharedPreferences.getInstance();

      // selectIndex 값 로드 (기본값으로 0 사용)
      bool rulletFlag = prefs.getBool('rulletFlag') ?? true;

      // 다음 화면으로 이동
      if(type == 'push') {
        rulletFlag ? Navigator.of(context).push(MaterialPageRoute(builder: (context) => RolletScreen(data: mergedList, categoryType: categoryType))) : Navigator.of(context).push(MaterialPageRoute(builder: (context) => ResultScreen(data: mergedList[selectIndex], categoryType: categoryType)));
      } else {
        rulletFlag ? Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => RolletScreen(data: mergedList, categoryType: categoryType))) : Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => ResultScreen(data: mergedList[selectIndex], categoryType: categoryType)));
      }

    } catch (e) {
      // 오류 처리
      print('Error loading JSON data: $e');
    }
  }
}