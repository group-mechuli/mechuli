import 'dart:io';

class AdHelper {

  // 배너광고
  static String get bannerAdUnitId {
    if (Platform.isAndroid) {
      return 'ca-app-pub-8189134534911724/9352078857';
    } else {
      throw UnsupportedError('Unsupported platform');
    }
  }

  // 전면광고
  static String get interstitialAdUnitId {
    if (Platform.isAndroid) {
      return 'ca-app-pub-8189134534911724/1494943599';
    } else {
      throw UnsupportedError('Unsupported platform');
    }
  }

  // 리워드광고
  static String get rewardedAdUnitId {
    // if (Platform.isAndroid) {
    //   return '<YOUR_ANDROID_REWARDED_AD_UNIT_ID>';
    // } else if (Platform.isIOS) {
    //   return '<YOUR_IOS_REWARDED_AD_UNIT_ID>';
    // } else {
      throw UnsupportedError('Unsupported platform');
    // }
  }
}
