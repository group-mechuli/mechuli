import 'package:flutter/material.dart';
import 'package:mechuli/screen/setting_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final double height;
  final String title;
  final bool homeFlag;
  const CustomAppBar({
    super.key,
    required this.height,
    required this.title,
    required this.homeFlag
  });

  // 값 로드하기
  _loadSavedValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    _loadSavedValue();
    return Container(
      width: double.infinity,
      height: height,
      decoration: const BoxDecoration(
          color: Color.fromRGBO(255, 92, 39, 1.0),
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0)
          )
      ),
      child: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                 !homeFlag ?
                 IconButton(
                    padding: const EdgeInsets.only(top: 50),
                    color: Colors.white,
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    iconSize: 24.0,
                  ) : SizedBox.shrink(),
                  IconButton(
                    padding: const EdgeInsets.only(top: 50),
                    color: Colors.white,
                    icon: Icon(Icons.settings_outlined),
                    onPressed: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (
                              context) => const SettingScreen()));
                    },
                    iconSize: 24.0,
                  ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 40),
              child: Center(
                child: Text(
                  title,
                  style: const TextStyle(
                    fontFamily: 'NanumPen',
                    fontSize: 24,
                    color: Color.fromRGBO(255, 255, 255, 1.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height);
}