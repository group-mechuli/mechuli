// import 'package:flutter/material.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';
//
// class BottomBanner extends StatefulWidget {
//   final String adUnitId;
//
//   const BottomBanner({Key? key, required this.adUnitId}) : super(key: key);
//
//   @override
//   BottomBannerState createState() => BottomBannerState();
// }
//
// class BottomBannerState extends State<BottomBanner> {
//   BannerAd? _bannerAd;
//
//   @override
//   void initState() {
//     super.initState();
//     _bannerAd = BannerAd(
//       adUnitId: widget.adUnitId,
//       request: AdRequest(),
//       size: AdSize.banner,
//       listener: BannerAdListener(
//         onAdLoaded: (Ad ad) {
//           setState(() {});
//         },
//         onAdFailedToLoad: (Ad ad, LoadAdError error) {
//           ad.dispose();
//         },
//       ),
//     )..load();
//   }
//
//   @override
//   void dispose() {
//     _bannerAd?.dispose();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return _bannerAd == null
//         ? SizedBox.shrink()
//         : Container(
//       alignment: Alignment.center,
//       width: _bannerAd!.size.width.toDouble(),
//       height: _bannerAd!.size.height.toDouble(),
//       child: AdWidget(ad: _bannerAd!),
//     );
//   }
// }