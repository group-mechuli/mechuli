// import 'package:flutter/material.dart';
// // import 'package:google_mobile_ads/google_mobile_ads.dart';
// import 'package:mechuli/screen/home_screen.dart';
//
// class CommonInterstitialAd extends StatefulWidget {
//   final String adUnitId;
//
//   const CommonInterstitialAd({Key? key, required this.adUnitId}) : super(key: key);
//
//   @override
//   _CommonInterstitialAdState createState() => _CommonInterstitialAdState();
// }
//
// class _CommonInterstitialAdState extends State<CommonInterstitialAd> {
//   InterstitialAd? _interstitialAd;
//   bool _isAdLoaded = false;
//
//   @override
//   void initState() {
//     super.initState();
//     _loadInterstitialAd();
//   }
//
//   void _loadInterstitialAd() {
//     InterstitialAd.load(
//       adUnitId: widget.adUnitId,
//       request: AdRequest(),
//       adLoadCallback: InterstitialAdLoadCallback(
//         onAdLoaded: (InterstitialAd ad) {
//           setState(() {
//             _interstitialAd = ad;
//             _isAdLoaded = true;
//           });
//         },
//         onAdFailedToLoad: (LoadAdError error) {
//           setState(() {
//             _isAdLoaded = false;
//           });
//           print('InterstitialAd failed to load: $error');
//         },
//       ),
//     );
//   }
//
//   void _showInterstitialAd() {
//     if (_interstitialAd != null) {
//       Navigator.of(context).push(
//           MaterialPageRoute(builder: (
//               context) => const HomeScreen()));
//       _interstitialAd!.fullScreenContentCallback = FullScreenContentCallback(
//         onAdDismissedFullScreenContent: (InterstitialAd ad) {
//           ad.dispose();
//           _loadInterstitialAd(); // Load a new ad
//         },
//         onAdFailedToShowFullScreenContent: (InterstitialAd ad, AdError error) {
//           ad.dispose();
//           _loadInterstitialAd(); // Load a new ad
//         },
//       );
//       _interstitialAd!.show();
//       _interstitialAd = null;
//       setState(() {
//         _isAdLoaded = false;
//       });
//     } else {
//       print('Interstitial ad is not loaded yet');
//       Navigator.of(context).push(
//           MaterialPageRoute(builder: (
//               context) => const HomeScreen()));
//     }
//   }
//
//   @override
//   void dispose() {
//     _interstitialAd?.dispose();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return ElevatedButton(
//       style: ElevatedButton.styleFrom(
//         shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(0)
//         ),
//         elevation: 0.0,
//         backgroundColor: Color.fromRGBO(78, 209, 78, 1.0), // 버튼의 배경색을 파란색으로 설정
//       ),
//       onPressed: _showInterstitialAd,
//       child: Text('결정', style: TextStyle(
//         fontFamily: 'NanumPen',
//         fontSize: 22,
//         color: Colors.white,
//       )),
//     );
//   }
// }