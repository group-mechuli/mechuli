import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsNotifier extends ChangeNotifier {
  bool _rulletFlag = false;
  bool get rulletFlag => _rulletFlag;

  bool _lunchPushFlag = false;
  bool get lunchPushFlag => _lunchPushFlag;

  SettingsNotifier() {
    _loadSettings();
  }

  Future<void> _loadSettings() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _rulletFlag = prefs.getBool('rulletFlag') ?? true;
    _lunchPushFlag = prefs.getBool('lunchPushFlag') ?? true;
    notifyListeners();
  }

  Future<void> setRulletFlag(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('rulletFlag', value);
    _rulletFlag = value;
    notifyListeners();
  }

  Future<void> setLunchPushFlag(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('lunchPushFlag', value);
    _lunchPushFlag = value;
    notifyListeners();
  }
}