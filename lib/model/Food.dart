class Food {
  final String name;
  final String brand;
  final String category;

  Food({
    required this.name,
    required this.brand,
    required this.category,
  });

  factory Food.fromJson(Map<String, dynamic> json) {
    return Food(
      name: json['name'],
      brand: json['brand'],
      category: json['category'],
    );
  }
}