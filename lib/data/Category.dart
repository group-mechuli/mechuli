class Category {
  final CategoryType categoryType;
  final String imagePath;

  Category(this.categoryType, this.imagePath);
}

enum CategoryType {
  ALL('all','전체'),
  CHICKEN('chicken', '치킨'),
  KOREAN('korean', '한식'),
  CHINA('china', '중식'),
  SUSHI('sushi', '일식'),
  HAMBURGER('hamburger', '햄버거'),
  PIZZA('pizza', '피자'),
  BOSSAM('bossam', '보쌈/족발'),
  JJIM('jjim', '찜/탕'),
  ASIAN('asian', '아시안'),
  BUNSIK('bunsik', '분식'),
  SALAD('salad', '샐러드')
  ;

  final String value;
  final String name;

  const CategoryType(this.value, this.name);

  static CategoryType fromString(String value) {
    return CategoryType.values.firstWhere((element) => element.value == value);
  }

}