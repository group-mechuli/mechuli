import 'package:mechuli/data/Category.dart';

final all = Category(CategoryType.ALL, 'images/category/0_all.png');
final chicken = Category(CategoryType.CHICKEN, 'images/category/1_chicken.png');
final korean = Category(CategoryType.KOREAN, 'images/category/2_korean.png');
final china = Category(CategoryType.CHINA, 'images/category/10_chiness.png');
final hamburger = Category(CategoryType.HAMBURGER, 'images/category/4_hambuger.png');
final pizza = Category(CategoryType.PIZZA, 'images/category/5_pizza.png');
final bossam = Category(CategoryType.BOSSAM, 'images/category/6_bossam.png');
final jjim = Category(CategoryType.JJIM, 'images/category/7_2__jjim.png');
final asian = Category(CategoryType.ASIAN, 'images/category/8_asian.png');
final sushi = Category(CategoryType.SUSHI, 'images/category/9_sushi.png');
final bunsik = Category(CategoryType.BUNSIK, 'images/category/11_bunsik.png');
final salad = Category(CategoryType.SALAD, 'images/category/12_salad.png');

final categories = [
  all,
  korean,
  china,
  sushi,
  chicken,
  hamburger,
  pizza,
  bossam,
  jjim,
  asian,
  bunsik,
  salad
];
