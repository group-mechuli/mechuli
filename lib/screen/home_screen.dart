import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mechuli/data/Category.dart';
import 'package:mechuli/data/CategoryDummy.dart';
import 'package:mechuli/util/data.dart';
// import 'package:mechuli/widget/bottom_banner.dart';
import 'package:mechuli/widget/custom_appbar.dart';
import 'category_screen.dart';
// import 'package:mechuli/ads/ad__helper.dart';

class HomeScreen extends StatefulWidget {
  final Category? category;

  const HomeScreen({this.category, super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  DateTime? currentBackPressTime;
  @override
  Widget build(BuildContext context) {
    final selectedCategory = widget.category ?? categories[0];
    return PopScope(
      canPop: false,
      onPopInvoked : (bool didPop){
        DateTime now = DateTime.now();
        if(currentBackPressTime == null || now.difference(currentBackPressTime!) > Duration(seconds: 2)) {

          currentBackPressTime = now;
          final msg = "'뒤로'버튼을 한 번 더 누르면 종료됩니다.";

          Fluttertoast.showToast(msg: msg);
        } else {
          SystemNavigator.pop();
        }
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          appBar: const CustomAppBar(
            height: 161, // AppBar의 높이
            title: '오늘은 어떤 음식 메뉴를 추천 드릴까요?',
            homeFlag: true,
          ),
          body: Stack(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 60),
                        child: Center(
                          child: Image.asset(
                              'images/logo.png', width: 140, height: 140),
                        ),
                      ),
                      const SizedBox(height: 30),
                      const Stack(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(right: 30.0),
                            child: Text('오늘의 메추리 픽?', style: TextStyle(
                              fontFamily: 'NanumPen',
                              fontSize: 32,
                              color: Color.fromRGBO(78, 209, 78, 1.0),
                            )),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 60.0, top: 30),
                            child: Text('오늘의 메추리 픽?', style: TextStyle(
                              fontFamily: 'NanumPen',
                              fontSize: 22,
                              color: Color.fromRGBO(143, 235, 158, 1.0),
                            )),
                          ),
                        ],
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            GestureDetector(
                              onTap: () =>
                              {
                                Navigator.of(context).push(
                                    MaterialPageRoute(builder: (
                                        context) => const CategoryScreen()))
                              },
                              child: Stack(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 18, bottom: 0),
                                    child: Center(child: Image.asset(
                                        selectedCategory.imagePath, width: 80, height: 80)),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 90),
                                    child: Center(
                                      child: Text("카테고리", style: const TextStyle(
                                        fontFamily: 'NanumPen',
                                        fontSize: 18,
                                        color: Color.fromRGBO(0, 0, 0, 1.0),
                                      )),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 0),
                              child: Center(
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    elevation: 0.0,
                                    backgroundColor: Color.fromRGBO(
                                        78, 209, 78, 1.0),
                                  ),
                                  onPressed: () {
                                    DataLoader.loadJsonData(context, CategoryType.ALL, 'push');
                                  },
                                  child: const Text('메추리 픽?', style: TextStyle(
                                    fontFamily: 'NanumPen',
                                    fontSize: 22,
                                    color: Colors.white,
                                  )),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  // Positioned(
                  //     bottom: 0,
                  //     left: 0,
                  //     right: 0,
                  //     child: Stack(
                  //       children: [
                  //         Container(
                  //           alignment: Alignment.center,
                  //           child: BottomBanner(adUnitId: AdHelper.bannerAdUnitId),
                  //         )
                  //       ]
                  //     )
                  // )
              ]
          )
      ),
    );
  }
}