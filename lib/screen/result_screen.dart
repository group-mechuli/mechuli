import 'package:flutter/material.dart';
import 'package:mechuli/ads/ad__helper.dart';
import 'package:mechuli/data/Category.dart';
import 'package:mechuli/model/Food.dart';
import 'package:mechuli/util/data.dart';
// import 'package:mechuli/widget/bottom_banner.dart';
import 'package:mechuli/widget/common_interstitial_ad.dart';

class ResultScreen extends StatelessWidget {
  final Food data;
  final CategoryType categoryType;
  const ResultScreen({Key? key, required this.data, required this.categoryType}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: true,
        elevation: 0.0,
      ),
      body: Stack(
        children: [
          Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset('images/result.png', width:170, height: 170),
                  SizedBox(height: 30),
                  Padding(padding: EdgeInsets.only(left: 20, right: 20),
                      child: Column(
                        children: [
                          Text('오늘의 메추리 픽?', style: TextStyle(
                            fontFamily: 'NanumPen',
                            fontSize: 24,
                            color: Color.fromRGBO(0, 0, 0, 1.0),
                          )),
                          SizedBox(height: 20),
                          Text(data.name,
                              textAlign: TextAlign.center,
                              softWrap:true,
                              style: TextStyle(
                                height: 0.9,
                                fontFamily: 'NanumPen',
                                fontSize: 48,
                                color: Color.fromRGBO(78, 209, 78, 1.0),
                              )
                          ),
                          Text('TM. ${data.brand}', style: TextStyle(
                            fontFamily: 'NanumPen',
                            fontSize: 30,
                            color: Color.fromRGBO(236, 89, 1, 1.0),
                          )),
                        ],
                      ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0)
                            ),
                            elevation: 0.0,
                            backgroundColor: Color.fromRGBO(181, 181, 181, 1.0), // 버튼의 배경색을 파란색으로 설정
                          ),
                          onPressed: () {
                            DataLoader.loadJsonData(context, categoryType, 'replace');
                          },
                          child: Text('다시', style: TextStyle(
                            fontFamily: 'NanumPen',
                            fontSize: 22,
                            color: Colors.white,
                          )),
                        ),
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.all(8.0),
                      //   child: CommonInterstitialAd(adUnitId: AdHelper.interstitialAdUnitId),
                      // )
                    ],
                  ),
                  SizedBox(height: 100,)
                ],
              )
          ),
          // Positioned(
          //     bottom: 0,
          //     left: 0,
          //     right: 0,
          //     child: Stack(
          //         children: [
          //           Container(
          //             alignment: Alignment.center,
          //             child: BottomBanner(adUnitId: AdHelper.bannerAdUnitId),
          //           )
          //         ]
          //     )
          // )
        ],
      ),
    );
  }
}