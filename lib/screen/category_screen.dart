import 'package:flutter/material.dart';
import 'package:mechuli/ads/ad__helper.dart';
import 'package:mechuli/data/Category.dart';
import 'package:mechuli/data/CategoryDummy.dart';
import 'package:mechuli/util/data.dart';
// import 'package:mechuli/widget/bottom_banner.dart';
import 'package:mechuli/widget/custom_appbar.dart';

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  State<CategoryScreen> createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(
          height: 161,
          title: '카테고리를 선택하세요.',
          homeFlag: false
      ),
      body: Stack(
        children: [
          GridView.builder(
            itemCount: categories.length,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                childAspectRatio: 0.1 / 0.15,
                mainAxisSpacing: 0,
                crossAxisSpacing: 0),
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                onTap: () {
                  DataLoader.loadJsonData(context, CategoryType.fromString(categories[index].categoryType.value) , 'push');
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 15, top: 40, right: 15, bottom: 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(categories[index].imagePath, width: 70, height: 70),
                      Container(
                        height: 30,
                        child: Text(
                          categories[index].categoryType.name,
                          style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'NanumPen'),
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
          // Positioned(
          //     bottom: 0,
          //     left: 0,
          //     right: 0,
          //     child: Stack(
          //         children: [
          //           Container(
          //             alignment: Alignment.center,
          //             child: BottomBanner(adUnitId: AdHelper.bannerAdUnitId),
          //           )
          //         ]
          //     )
          // )
        ],
      ),
    );
  }
}
