import 'package:flutter/material.dart';
// import 'package:mechuli/ads/ad__helper.dart';
import 'package:mechuli/main.dart';
import 'package:mechuli/notification/local_notification.dart';
import 'package:mechuli/storage/SettingsNotifier.dart';
// import 'package:mechuli/widget/bottom_banner.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingScreen extends StatefulWidget {
  const SettingScreen({Key? key}) : super(key: key);

  @override
  State<SettingScreen> createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {

  // bool _rulletFlag = false;
  // bool _lunchPushFlag = false;

  @override
  void initState() {
    super.initState();
  }

  // 값 저장하기
  setRulletFlag(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('rulletFlag', value);
  }

  // 값 저장하기
  setLunchPushFlag(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('lunchPushFlag', value);
  }

  @override
  Widget build(BuildContext context) {
    final settings = Provider.of<SettingsNotifier>(context);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 92, 39, 1.0),
        iconTheme: IconThemeData(color: Colors.white),
        automaticallyImplyLeading: true,
        elevation: 0.0,
      ),
      body: Stack(
        children: [
          Padding(
              padding: EdgeInsets.only(left: 20, top: 50, right: 10, bottom: 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("설정", style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold
                  ),
                  ),
                  Divider(
                    height: 40, // 구분선의 높이 조절 가능
                    thickness: 1, // 구분선의 두께 조절 가능
                    color: Colors.grey, // 구분선의 색상 설정 가능
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("랜덤 뽑기 룰렛 애니메이션", style: TextStyle(
                        fontSize: 16,
                      )
                      ),
                      Switch(
                        inactiveThumbColor: Colors.white,
                        inactiveTrackColor: Colors.grey,
                        activeTrackColor: Colors.redAccent,
                        value: settings.rulletFlag,
                        onChanged: (value) {
                          settings.setRulletFlag(value);
                        },
                      )
                    ],
                  ),
                  Divider(
                    height: 10, // 구분선의 높이 조절 가능
                    thickness: 1, // 구분선의 두께 조절 가능
                    color: Colors.grey, // 구분선의 색상 설정 가능
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("점심 랜덤 뽑기 알림",
                          style: TextStyle(
                            fontSize: 16,
                          )
                      ),
                      Switch(
                        inactiveThumbColor: Colors.white,
                        inactiveTrackColor: Colors.grey,
                        activeTrackColor: Colors.redAccent,
                        value: settings.lunchPushFlag,
                        onChanged: (value) async {
                          settings.setLunchPushFlag(value);
                          // cancel the notification with id value of zero
                          print(value);
                          if(value) {
                            LocalNotification.showNotification();
                          } else {
                            await flutterLocalNotificationsPlugin.cancel(0);
                          }
                        },
                      )
                    ],
                  )
                ],
              )
          ),
          // Positioned(
          //     bottom: 0,
          //     left: 0,
          //     right: 0,
          //     child: Stack(
          //         children: [
          //           Container(
          //             alignment: Alignment.center,
          //             child: BottomBanner(adUnitId: AdHelper.bannerAdUnitId),
          //           )
          //         ]
          //     )
          // )
        ],
      ),
    );
  }
}
