import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:permission_handler/permission_handler.dart';
import 'home_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    _permissionWithNotification();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);

    Future.delayed(Duration(seconds: 3), () {
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => const HomeScreen()));
    });

    //_scheduleNotification();
  }

  void _permissionWithNotification() async {
    if (await Permission.notification.isDenied &&
        !await Permission.notification.isPermanentlyDenied) {
      await [Permission.notification].request();
    }
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Image.asset('images/logo.png', width:160, height: 160),
                ),
                Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 50.0),
                      child: Text('MECHULI', style: TextStyle(
                        fontFamily: 'NanumPen',
                        fontSize: 32,
                        color: Color.fromRGBO(78, 209, 78, 1.0),
                      )),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 60.0, top: 30),
                      child: Text('메뉴를 골라줄께', style: TextStyle(
                        fontFamily: 'NanumPen',
                        fontSize: 22,
                        color: Color.fromRGBO(143, 235, 158, 1.0),
                      )),
                    ),
                  ],
                ),
              ],
            ),
            Positioned(
              bottom: 10,
              left: 0,
              right: 0,
              child: Center(
                child: Text('© niceday',
                    style: TextStyle(
                    fontSize: 12,
                    color: Color.fromRGBO(199, 199, 199, 1.0),
                  )
                ),
              )
            )
          ],
        )
    );
  }

  /*Future<void> _scheduleNotification() async {
    tz.initializeTimeZones();
    final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
    final tz.TZDateTime scheduledDate = tz.TZDateTime(tz.local, now.year, now.month, now.day, 18, 10); // 오늘 오후 2시로 설정

    await flutterLocalNotificationsPlugin.zonedSchedule(
      0,
      '제목',
      '알림 내용',
      scheduledDate,
      const NotificationDetails(
        android: AndroidNotificationDetails(
          'your channel id',
          'your channel name',
          channelDescription: 'your channel description',
          importance: Importance.max,
          priority: Priority.high,
          showWhen: false,
        ),
      ),
      androidAllowWhileIdle: true,
      uiLocalNotificationDateInterpretation:
      UILocalNotificationDateInterpretation.absoluteTime,
      matchDateTimeComponents: DateTimeComponents.time,
    );
  }*/
}
