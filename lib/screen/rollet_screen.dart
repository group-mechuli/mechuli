import 'dart:ui';

import 'package:flutter/material.dart';
// import 'package:mechuli/ads/ad__helper.dart';
import 'package:mechuli/constants/constants.dart';
import 'package:mechuli/data/Category.dart';
import 'package:mechuli/model/Food.dart';
import 'package:mechuli/screen/result_screen.dart';
// import 'package:mechuli/widget/bottom_banner.dart';

class RolletScreen extends StatefulWidget {
  final List<Food> data;
  final CategoryType categoryType;
  const RolletScreen({Key? key, required this.data, required this.categoryType}) : super(key: key);

  @override
  State<RolletScreen> createState() => _RolletScreenState();
}

class _RolletScreenState extends State<RolletScreen> {
  final ScrollController _scrollController = ScrollController();
  int _selectedIndex = 0;
  String result = '';
  void _scrollToCenter(int index) {
    setState(() {
      _selectedIndex = index;
    });

    double scrollTo = (112.0 / 2) * index;

    _scrollController.animateTo(
      scrollTo,
      duration: Duration(seconds: 5),
      curve: Curves.easeInOut,
    );

    Future.delayed(Duration(seconds: 6), () {
      return widget.data[selectIndex];
    }).then((value) {
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => ResultScreen(data: value, categoryType: widget.categoryType)));
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      widget.data;
      _scrollToCenter(selectIndex-2); // Scroll to index 50
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 92, 39, 1.0),
        iconTheme: IconThemeData(color: Colors.white),
        automaticallyImplyLeading: false,
        elevation: 0.0,
      ),
      body: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 0, bottom: 0),
                child: Center(
                  child:Image.asset('images/logo.png', width:120, height: 120),
                ),
              ),
              SizedBox(height: 20),
              Stack(
                children: [
                  Container(
                  height: 286,
                  color: Color.fromRGBO(255, 255, 255, 0.5),
                  child: ListView.builder(
                    controller: _scrollController,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      int itemIndex = index % widget.data.length; // Set the desired number of items
                      return  Stack(
                        children: [
                          Center(
                            child: ListTile(
                              // tileColor: Colors.blue,
                              // contentPadding: EdgeInsets.fromLTRB(160, 0, 0, 0),
                              title: Center(
                                  child:  Text(
                                    maxLines: 1,
                                    '${widget.data[itemIndex].name}',
                                    style: TextStyle(
                                        fontFamily: 'NanumPen',
                                        fontSize: 22,
                                        color: Color.fromRGBO(143, 235, 158, 1.0)
                                    ),
                                  )
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
                Positioned(
                  top: 110,
                  left: 10,
                  right: 10,
                  child: Container(
                    height: 60,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.red, // 선의 색상
                        width: 4, // 선의 두께
                      ),
                    ),
                  ),
                ),
                ]
              ),
            ],
          ),
          // Positioned(
          //     bottom: 0,
          //     left: 0,
          //     right: 0,
          //     child: Stack(
          //         children: [
          //           Container(
          //             alignment: Alignment.center,
          //             child: BottomBanner(adUnitId: AdHelper.bannerAdUnitId),
          //           )
          //         ]
          //     )
          // )
        ],
      ),
    );
  }
}
